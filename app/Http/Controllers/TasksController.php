<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller

{


	public function indexx()
    {
        $titles = Task::paginate(5);
        return view('tasks.index', compact('titles'));
    }
    //
    public function index(){
    	$tasks = Task::all();

    	return view('tasks.index',['tasks'=>$tasks]);
    }

    public function create(){
    	return view('tasks.create');
    }

    public function store(Request $req){

    	$task = new Task;	
    	$task->fill($req->all());
    	$task->save();

    	return redirect()->route('tasks.index');	    
   	}

   	public function edit($id){
   		$task = Task::find($id);

   		return view('tasks.edit', ['task' => $task]);
   	}


   	public function update(Request $req, $id){
   		$task = Task::find($id);

   		$task->fill($req->all());
   		$task->save();

   		return redirect()->route('tasks.index');
   	}



   	





}

@extends('welcome')

@section('content')
<div class="jumbotron">
	<div class="container">

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
					<h3>Коментарии</h3>

					<a href="{{route('tasks.create')}}" class="btn btn-default">Создать</a>
		<table class="table">
				<thead>
						<td>ID</td>
						<td>Тема</td>
						<td>Просмотреть, редактировать</td>
					</tr>
				</thead>
				<tbody>
			<!-- Вывод -->
				@foreach($tasks as $task)
					<tr>
						<td>
							{{$task->id}}
						</td>

						<td>
							{{$task->title}}
						</td>

						<td>
						<a href="{{route('tasks.edit',$task->id)}}">
								<i class ="glyphicon glyphicon-edit"></i>
							</a>	
					</td>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>


</div>

@endsection